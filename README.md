# Arkansongs Handler

This program handles Arkansongs for Zac at kuhsradio.org.

It:

Checks for new show directories in .../Arkansongs. (via "showname MM-DD-YY" or "showname MM-DD-YYYY" format)

Converts largest .wav file in those directories to "YYYY-MM-DD showname"

Checks for new show directories in .../Arkansongs (via "showname MM-DD-YY" or "showname MM-DD-YYYY" format).

Converts largest .wav file in those directories to "YYYY-MM-DD showname".

Moves those .wav files to .../Arkansongs/_shows.

Backs up last week's show to .../Arkansongs/_backup.

Queues up next show in line as .../Arkansongs/arkansongs.wav.

Creates current show info in .../Arkansongs/queue_info.txt file.

Cleans up after itself.

Some day I will learn about markdown.
