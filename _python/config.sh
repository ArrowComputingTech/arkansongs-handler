#!/bin/bash
# This bash shell script creates an example file/folder structure necessary for arkansongs.py to work.
touch ../arkansongs.wav
touch ../queue_info.txt
rm -r ../"Hambone Willie for 10.25.19"
mkdir ../"Hambone Willie for 10.25.19"
rm -r ../"Charlie for 12.22.2019"
mkdir ../"Charlie for 12.22.2019"
rm -r ../"Cripps Combo for 11.22.19"
mkdir ../"Cripps Combo for 11.22.19"
touch ../"Hambone Willie for 10.25.19"/some_song.wav
touch ../"Hambone Willie for 10.25.19"/next_song.wav
echo "wufjwiefjuwegiuehgiuwheiufhwiuehfiuwheifuhwieuhfw" >> ../"Hambone Willie for 10.25.19"/a_song.wav
touch ../"Cripps Combo for 11.22.19"/song01.wav
touch ../"Cripps Combo for 11.22.19"/song02.wav
echo "uijfuwduifeufjwehwruwufjwiefjuwegiuehgiuwheiufhwiuehfiuwheifuhwieuhfw" >> ../"Cripps Combo for 11.22.19"/song03.wav
touch ../_backup/arkansongs.wav
