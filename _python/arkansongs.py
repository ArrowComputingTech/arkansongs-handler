import os, shutil, re, glob
from datetime import datetime

show_dir = '/Volumes/KUHS Music/DJ Audio Files/Arkansongs'
#show_dir = "/ZOne/_work/Josh/KUHS/python/arkansongs"
backup_dir = os.path.join(show_dir, '_backup')

class HandleData():
    def __init__(self):
        self.delete_dirs = []

    def mainLoop(self):
    # Move largest files from each directory that is named with a date into _shows folder,
    # creating file names based on directory date (YYYY-MM-DD format) + filename.
        os.chdir(show_dir)
        if os.path.isfile('./arkansongs.wav'): # Fail if arkansongs.wav not found - should always be in external/.../Arkansongs base directory.
            moved_files = self.handleDirs()
            if not (moved_files):
                print("No new files found...")
            else:
                self.cleanupDirs()
            self.handleBackup()
            self.queueShow()
        else:
            print ('arkansongs.wav not found...aborting...')

    def handleDirs(self):
        moved_files = False
        for root, dirs, files in os.walk(show_dir):
            for directory in dirs:
                largest_file = ""
                file_date = self.handleFileDate(directory)
                if file_date != "" and file_date != None:
                    date_string = self.handleDateString(file_date)
                    largest_file = self.handleSubDirs(directory, largest_file)
                    if largest_file != "":
                        moved_files = self.moveFile(date_string, largest_file)
        return moved_files

    def handleFileDate(self, directory):
    # Set file_date with date pattern from regex.
        file_date = re.search("[0-9][0-9]?[-. ][0-9][0-9]?[-. ][0-9]{2}([0-9]{2})?", directory.strip())
        # Process file_date if there is a result, otherwise next handleDirs loop will be called.
        if file_date != "" and file_date != None:
            # Set file date to actual date derived from regex and replace periods with dashes.
            # !!! Check to see if this works for spaces, replace spaces also.
            file_date = file_date.group(0).replace('.', '-')
            file_date = file_date.replace(' ', '-')
        else:
            print ('Directory: ' + directory + ' does not contain a date. Skipping.')
        return file_date

    def handleDateString(self, file_date):
    # Convert file_date to M-D-Y format.
        print(file_date)
        date_string = datetime.strptime(file_date, '%m-%d-%y')
        return date_string

    def handleSubDirs(self, directory, largest_file):
    # Get files in subdirs of directory.
        for subfile in glob.glob(os.path.join(show_dir, directory, '*')):
            current_dir = os.path.join(show_dir + '/' + directory)
            # Set initial largest_file (file to be moved to _shows from directory.
            if largest_file == "":
                largest_file = subfile
                self.deleteDir(current_dir)
            else:
                # Set largest file for current subfile if appropriate.
                if os.path.getsize(subfile) > os.path.getsize(largest_file):
                    self.handleDeleteDir(current_dir)
                    largest_file = subfile
                else:
                    self.handleDeleteDir(current_dir)
        return largest_file

    def deleteDir(self, dir):
    # Appends this dir to delete_dirs for later deletion.
        self.delete_dirs.append(dir)

    def handleDeleteDir(self, current_dir):
    # Add directory to self.delete_dirs if not already done.
        if current_dir not in self.delete_dirs:
            self.deleteDir(current_dir)

    def moveFile(self, date_string, largest_file):
    # Move largest file to _shows.
        file_name = str(date_string.strftime('%Y')) + '-' + str(date_string.strftime('%m')) + '-' + str(date_string.strftime('%d')) + ' ' + os.path.basename(largest_file)
        print('Moving ' + largest_file + ' to _shows as: ' + file_name)
        os.rename(largest_file, os.path.join(show_dir, '_shows', file_name))
        return True

    def cleanupDirs(self):
    # Remove extraneous directories/files.
        for dir in self.delete_dirs:
            print('Deleting directory: ' + dir)
            shutil.rmtree(dir)

    def handleBackup(self):
    # Delete old backup from _backup.
        if os.path.exists(os.path.join(backup_dir, 'arkansongs.wav')):
            os.remove(os.path.join(backup_dir, 'arkansongs.wav'))
        else:
            print("Backup of last week's show not found.")
        # Move current arkansongs.wav to /backup.
        shutil.move(os.path.join(show_dir, 'arkansongs.wav'), backup_dir)

    def queueShow(self):
    # Handles this week's show.
        os.chdir('./_shows')
        shows = glob.glob('./*.wav')
        # Check if there are any new shows.
        if len(shows) > 0:
            shows.sort()
            # Copy oldest /_shows/.wav to /arkansongs.wav.
            shutil.move(shows[0], '../arkansongs.wav')
            print('Made ' + shows[0] + ' the current arkansongs.wav.')
            self.handleQueueInfo(shows)
        else:
            print("No new show found. Using last week's show.")

    def handleQueueInfo(self, shows):
    # Create /queue_info.txt with information about current arkansongs.wav.
        queue_file = '../queue_info.txt'
        if os.path.exists(queue_file):
            os.remove(queue_file)
        with open(queue_file, "w+") as queuefile:
            queuefile.write(shows[0])

print ('Preparing to organize Arkansongs files...')
hd = HandleData()
hd.mainLoop()
